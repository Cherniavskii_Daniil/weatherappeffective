package com.example.weatherappeffectieve.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.weatherappeffectieve.model.WeatherModel
import com.example.weatherappeffectieve.service.WeatherAPIService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

private const val TAG = "MainViewModel"

class MainViewModel : ViewModel() {

    private val weatherApiService = WeatherAPIService() // Отвечает за API
    private val disposable = CompositeDisposable()

    val loading = MutableLiveData<Boolean>()        // Загрузка данных
    val response = MutableLiveData<WeatherModel>()  // Все прошло нормально
    val error = MutableLiveData<Boolean>()          // Ошибка

    private fun getDataFromAPI(cityName: String) {
        loading.value = true

        disposable.add(
            weatherApiService.getDataService(cityName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<WeatherModel>() {
                    // Данные получили, запись в value
                    override fun onSuccess(t: WeatherModel) {
                        response.value = t
                        error.value = false
                        loading.value = false
                        Log.d(TAG, "onSuccess: Success")
                    }

                    // При получении данных произошла ошибка
                    override fun onError(e: Throwable) {
                        error.value = true
                        loading.value = false
                        Log.e(TAG, "onError: $e")
                    }
                })
        )
    }

    fun refreshData(cityName: String) {
        getDataFromAPI(cityName)
    }

}