package com.example.weatherappeffectieve.model

data class Wind(
    val deg: Int,
    val speed: Double
)