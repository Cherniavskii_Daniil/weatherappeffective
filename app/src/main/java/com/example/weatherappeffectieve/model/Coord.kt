package com.example.weatherappeffectieve.model


data class Coord(
    val lat: Double,
    val lon: Double
)