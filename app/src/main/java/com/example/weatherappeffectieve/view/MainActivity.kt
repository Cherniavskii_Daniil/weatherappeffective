package com.example.weatherappeffectieve.view

import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.weatherappeffectieve.R
import com.example.weatherappeffectieve.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

private const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

    private lateinit var viewmodel: MainViewModel

    private lateinit var GET: SharedPreferences
    private lateinit var SET: SharedPreferences.Editor

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        GET = getSharedPreferences(packageName, MODE_PRIVATE)
        SET = GET.edit()

        viewmodel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        val city = GET.getString("cityName", "omsk")?.toLowerCase()
        edt_city_name.setText(city)
        viewmodel.refreshData(city!!)

        getLiveData()

        swipe_refresh_layout.setOnRefreshListener {
            ll_data.visibility = View.GONE
            tv_error.visibility = View.GONE
            pb_loading.visibility = View.GONE

            val cityName = GET.getString("cityName", city)?.toLowerCase()
            edt_city_name.setText(cityName)
            viewmodel.refreshData(cityName!!)
            swipe_refresh_layout.isRefreshing = false
        }

        img_search_city.setOnClickListener {
            val cityName = edt_city_name.text.toString()
            SET.putString("cityName", cityName)
            SET.apply()
            viewmodel.refreshData(cityName)
            getLiveData()
            Log.i(TAG, "onCreate: " + cityName)
        }
    }

    private fun getLiveData() {

        // Данные получили
        viewmodel.response.observe(this, Observer { data ->
            data?.let {
                ll_data.visibility = View.VISIBLE

                // Название города
                main_address.text = data.name

                // Получение картинки погоды соотвествующей
                Glide.with(this)
                    .load("https://openweathermap.org/img/wn/" + data.weather.get(0).icon + "@2x.png")
                    .into(pictures_weather)

                main_humidity.text = data.main.humidity.toString() + "%"
                main_wind_speed.text = data.wind.speed.toString() + " км/ч"
                // Температура
                main_temp.text = (data.main.temp.toInt()).toString() + "°C"
                main_feels_temp.text = (data.main.feelsLike.toInt()).toString() + "°C"
                main_temp_max.text = data.main.tempMax.toInt().toString() + "°C"
                main_temp_min.text = data.main.tempMin.toInt().toString() + "°C"

            }
        })

        // При получении данных произошла ошибка
        viewmodel.error.observe(this, Observer { error ->
            error?.let {
                if (error) {
                    tv_error.visibility = View.VISIBLE
                    pb_loading.visibility = View.GONE
                    ll_data.visibility = View.GONE
                } else {
                    tv_error.visibility = View.GONE
                }
            }
        })

        // Процесс получения данных
        viewmodel.loading.observe(this, Observer { loading ->
            loading?.let {
                if (loading) {
                    pb_loading.visibility = View.VISIBLE
                    tv_error.visibility = View.GONE
                    ll_data.visibility = View.GONE
                } else {
                    pb_loading.visibility = View.GONE
                }
            }
        })

    }
}