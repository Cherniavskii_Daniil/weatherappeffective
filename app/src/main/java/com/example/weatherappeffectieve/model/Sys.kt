package com.example.weatherappeffectieve.model

data class Sys(
    val country: String,
    val sunrise: Int,
    val sunset: Int
)