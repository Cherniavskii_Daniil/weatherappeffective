package com.example.weatherappeffectieve.service

import com.example.weatherappeffectieve.model.WeatherModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherAPI {

    // https://api.openweathermap.org/
    // data/2.5/weather&appid=43465c998e86e00e7d1253c4445cccf9&units=metric&lang=ru

    @GET("data/2.5/weather?&units=metric&APPID=43465c998e86e00e7d1253c4445cccf9&lang=ru")
    fun getData(
        @Query("q") cityName: String
    ): Single<WeatherModel>

}